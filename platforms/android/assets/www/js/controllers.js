angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.isProvider = false;
  
  $scope.showProviderFun = function(type)
  {
	  if ($localStorage.provider == 0)
		  $scope.isProvider = false;
	  else
		  $scope.isProvider = true;
  }
  
  $scope.showProviderFun();
  
  
  $scope.isLoggedin = function()
  {
	  if ($localStorage.userid)
		$scope.userloggedIn = true;
	else
		$scope.userloggedIn = false;
  }
  
  $scope.isLoggedin();
  
  
  
  $scope.logOut = function()
  {
    $localStorage.userid = '';
	$localStorage.facebookid = '';
    $localStorage.name = '';
    $localStorage.email = '';	  
	$localStorage.phone = '';
	$localStorage.address = '';
	$localStorage.category = '';
	$localStorage.city = '';
	$localStorage.website = '';
	$localStorage.facebook = '';
	$localStorage.desc = '';
	$localStorage.provider= '';
	$localStorage.available = '';
	$scope.isLoggedin();
	
	  window.location = "#/app/loginoptions"
  }

  $scope.onSuccess = function()
  {
	  
  }  

  $scope.onError = function()
  {
	  
  }  
  $scope.ShareOptions = function()
  {
		window.plugins.socialsharing.available(function(isAvailable) {
		  // the boolean is only false on iOS < 6
		  if (isAvailable) {
				window.plugins.socialsharing.share($localStorage.name+' מזמין אותך לנסות את אפליקציית WhoCan..', null, null, 'http://whocan.com'),
			  $scope.onSuccess(), // optional success function
			  $scope.onError()   // optional error function
		  }
		});	
  }
 
})



.controller('LoginCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$state,$q,$ionicLoading,$ionicHistory) 
{
	$scope.navTitle= '<p class="Headtitle">עמוד התחברות<p>';
	
	$scope.loginfields = 
	{
		"email" : "",
		"password" : ""
	}
	
	
	$scope.RegisterBtn = function()
	{
		window.location = "#/app/register"
	}
	
	
	$scope.LoginBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.loginfields.email =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין כתובת מייל',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if (emailRegex.test($scope.loginfields.email) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת מייל לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.loginfields.email =  '';
		}	
		else if ($scope.loginfields.password =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין סיסמה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			send_params = 
			{
				"email" : $scope.loginfields.email,
				"password" : $scope.loginfields.password,
				"push_id" : $rootScope.pushId
			}
			console.log(send_params)
			$http.post($rootScope.Host+'/UserLogin',send_params)
			.success(function(data, status, headers, config)
			{
				console.log(data)
				if (data.response.status == 1)
				{
					
					$localStorage.userid = data.response.userid;
					$localStorage.name = data.response.name;
					$localStorage.email = data.response.email;

					
					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.category = data.response.category;
					$localStorage.city = data.response.city;
					//$localStorage.website = data.response.website;
					//$localStorage.facebook = data.response.facebook;
					//$localStorage.desc = data.response.desc;
					
					
					$localStorage.available = data.response.available;		
					
					//alert ($localStorage.available)
					
					
					$localStorage.provider = data.response.provider;		

					
					$scope.showProviderFun($localStorage.provider);					
					$scope.isLoggedin();
					//$localStorage.phone = data.response.phone;
					//$localStorage.image = data.response.image;
					
					
					$scope.loginfields.email = '';
					$scope.loginfields.password = '';
					
					window.location.href = "#/app/main";				
					
				}
				else
				{
					$ionicPopup.alert({
					title: 'מייל או סיסמה שגוים יש לנסות שוב',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

					$scope.loginfields.password = '';				   
				}

			})
			.error(function(data, status, headers, config)
			{

			});
		}
	}
})



.controller('IntroCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$state,$q,$ionicLoading,$ionicHistory) {

alert("shay")

})
.controller('LoginOptionsCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$state,$q,$ionicLoading,$ionicHistory) {

	//$localStorage.enterScreenIsSeen = '';
	
	if ($localStorage.userid)
	{
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			expire: 300
		});
		$state.go('app.main');	
		
	}
	
	else 
	{
		if (!$localStorage.enterScreenIsSeen)
		{
			$ionicHistory.nextViewOptions({
				disableAnimate: true,
				expire: 300
			});
			$localStorage.enterScreenIsSeen = true;	
			$state.go('app.intro');		
		}
		
		/*
		else  ($localStorage.enterScreenIsSeen)
		{
			$ionicHistory.nextViewOptions({
				disableAnimate: true,
				expire: 300
			});
			$state.go('app.loginoptions');		
		}
		*/
	}


	
	
	
	
	/*
	if ($localStorage.userid)
	{
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			expire: 300
		});
			
		$state.go('app.main');
	}
	*/
	
	$scope.loginfields = 
	{
		"email" : "",
		"password" : ""
	}
	
		
	$scope.EmailLoginBtn = function()
	{
		window.location = "#/app/login"
	}
	
	
  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {


	
	$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);

	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,gender,first_name,last_name,locale&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.FaceBookLoginBtn = function() {
	  
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);


					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

							
						$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);
							


						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				//console.log('getLoginStatus', success.status);
 
				$ionicLoading.show({
				  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
				});
 

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };


  $scope.FacebookLoginFunction = function(id,firstname,lastname,email,gender)
  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		$scope.fullname = firstname+' '+lastname;
		$scope.gender = (gender == "male" ? "זכר" : "נקבה");
			
			facebook_data = 
			{
				"id" : id,
				"firstname" : firstname,
				"lastname" : lastname,
				"email" : email,
				"fullname" : $scope.fullname,
				"gender" : $scope.gender,
				"push_id" : $rootScope.pushId 
			}					
			$http.post($rootScope.Host+'/facebookConnect',facebook_data).success(function(data)
			{
				//alert (data);
				//alert (data.response.userid)
			//	if (data.response.userid)
			//	{
				
				
					$localStorage.userid = data.response.userid;
					$localStorage.facebookid = id;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;
					
					
					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.category = data.response.category;
					$localStorage.city = data.response.city;
					//$localStorage.website = data.response.website;
					//$localStorage.facebook = data.response.facebook;
					//$localStorage.desc = data.response.desc;
					$localStorage.provider = data.response.provider;
					$localStorage.available = data.response.available;	
					
					$scope.showProviderFun();
					$scope.isLoggedin();
					$state.go('app.main');	

			});
		
	  
  }  

	

})

.controller('ForgotPassCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$state,$q,$ionicLoading) {

	$scope.navTitle= '<p class="Headtitle">שכחתי סיסמה<p>';
	
	
	$scope.forgot  = 
	{
		"email" : ""
	}
	
	$scope.sendPassword = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		var emailRegex = /\S+@\S+\.\S+/;



		
		if ($scope.forgot.email =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין מייל',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if (emailRegex.test($scope.forgot.email) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת מייל לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.forgot.email =  '';
		}	

		else
		{
			send_params2 = 
			{
				"email" : $scope.forgot.email,
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/ForgotPass',send_params2)
			.success(function(data, status, headers, config)
			{
				//alert (data.response.status);
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					title: 'כתובת מייל לא נמצאה במערכת יש לנסות שוב',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

					$scope.forgot.email = '';
								
					
				}
				else
				{
					
					$ionicPopup.alert({
					title: 'סיסמתך לאפליקציה נשלחה למייל שהזנת',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

				   $scope.forgot.email = '';
				   
				   
					window.location.href = "#/app/login";	
				}

			})
			.error(function(data, status, headers, config)
			{

			});
		}		
	}

})

.controller('RegisterCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$state,$q,$ionicLoading) {

	$scope.navTitle= '<p class="Headtitle">עמוד הרשמה<p>';
	
	$scope.loginfields = 
	{
		"email" : "",
		"password" : ""
	}
	
	$scope.registerfields = 
	{
		"name" : "",
		"email" : "",
		"password" : ""
	}
	

	
	$scope.registerUser = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		var emailRegex = /\S+@\S+\.\S+/;

		if ($scope.registerfields.name =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין שם מלא',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}

		
		else if ($scope.registerfields.email =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין מייל',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if (emailRegex.test($scope.registerfields.email) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת מייל לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.registerfields.email =  '';
		}	
		else if ($scope.registerfields.password =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין סיסמה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			send_params2 = 
			{
				"name" : $scope.registerfields.name,
				"email" : $scope.registerfields.email,
				"password" : $scope.registerfields.password,
				"push_id" : $rootScope.pushId
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/NewUser',send_params2)
			.success(function(data, status, headers, config)
			{
				//alert (data.response.status);
				if (data.response.status == 0)
				{
					//alert ( data.response.userid);
					
					$scope.registerfields.name = '';
					$scope.registerfields.email = '';
					$scope.registerfields.password = '';
					
					$localStorage.userid = data.response.userid;
					$localStorage.name = $scope.registerfields.name;
					$localStorage.email = $scope.registerfields.email;

					$localStorage.phone = data.response.phone;
					$localStorage.address = data.response.address;
					$localStorage.category = data.response.category;
					$localStorage.city = data.response.city;
					//$localStorage.website = data.response.website;
					//$localStorage.facebook = data.response.facebook;
					//$localStorage.desc = data.response.desc;
					$localStorage.provider = data.response.provider;
					$localStorage.available = data.response.available;						
					
					//$localStorage.phone = data.response.phone;
					//$localStorage.image = data.response.image;

					$scope.showProviderFun();
					$scope.isLoggedin();
					window.location.href = "#/app/main";				
					
				}
				else
				{
					$ionicPopup.alert({
					title: 'כתובת מייל כבר בשימוש יש לנסות שוב או להתחבר באמצעות מייל',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

					$scope.registerfields.email = '';
				}

			})
			.error(function(data, status, headers, config)
			{

			});
		}
	}
	
	$scope.backLogin = function()
	{
		window.location = "#/app/login"
	}

	$scope.RegisterBtn = function()
	{
		window.location = "#/app/register"
	}

})




.controller('MainCtrl', function($scope,$rootScope,$http,$ionicModal,$ionicPopup,$localStorage,$cordovaGeolocation,$compile,$ionicScrollDelegate,$filter, $timeout,$ionicSlideBoxDelegate,ClosePopupService,$cordovaSms) {

	$scope.phpHost = $rootScope.PHPHost;
	$scope.showHidden = 0;
	$scope.CatagoryIndex = 0;
	$scope.navTitle= '<img class="title-image" src="img/headerlogo.png" style="width:125px; margin-left:-15px; margin-top:2px;"/>';
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
	$scope.showInfo = true;
	$scope.showReviews  = false;
	$scope.showFindPro = true;
	$scope.showAddLocation = false;
	$scope.showProfession = false;
	$scope.suppliersCount = 0;
	$scope.categories = "";
	$scope.reportId = "";
	$scope.selectedCategory = 1;
	$scope.commentRating = 0;
	$scope.arrangeSizesCounts = 5;
	$scope.arrangeSizesMap = 1.6494845360824741;
	$scope.arrangeSizes = function() {
		if(document.getElementsByClassName('mapWrapper').length>0){
			document.getElementsByClassName('mapWrapper')[0].style='height:'+(window.innerHeight/$scope.arrangeSizesMap)+'px';
		}
			if (!$localStorage.enterMainScreen){
				$localStorage.enterMainScreen	= true;
				if(document.getElementsByClassName('chooseCatPopUp').length>0){
					setTimeout(function(){
						document.getElementsByClassName('chooseCatPopUp')[0].style.opacity='1';
						setTimeout(function(){
							document.getElementsByClassName('chooseCatPopUp')[0].style.opacity='0';
						}, 5000);
					}, 1500);
				}
				
			} 
			else{
				$localStorage.enterMainScreen	= false;
			}
			if($scope.arrangeSizesCounts>=0){
				$scope.arrangeSizesCounts--;
				setTimeout(function(){
								$scope.arrangeSizes();
						}, 1000);
			}
		
	};
		$scope.arrangeSizes();
		
	//first time entering category screen.
	
	//$localStorage.enterMainScreen = '';

	
	
	$scope.enterMainScreen = $localStorage.enterMainScreen;
	
	
	//$scope.Comments = [];
	
	$scope.Bar =
	{
		"open":false,
		"footerOpen":false
	}
	
	
	$scope.fields = 
	{
		"textmsg" : "",
		"comment" : "",
		"supplierindex" : "",
		"customsearch" : "0",
		"newlocation" : "",
		"searchradious" : $rootScope.DefaultRadius, 
		"location_lat" : "",
		"location_lng" : "",
		"locationname" : "",
		"reporttext" : "",
		"showdetails" : false
	}
	
	
	$scope.onDropComplete1=function(data,evt){
			alert("Stop Drag23")
            var index = $scope.droppedObjects1.indexOf(data);
            if (index == -1)
            $scope.droppedObjects1.push(data);
        }
		
	 $scope.onDragSuccess1=function(data,evt){
		 	alert("Stop Drag")
            var index = $scope.droppedObjects1.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects1.splice(index, 1);
            }
        }
	
	
	$scope.moveInfoPopUp = function() {
        var buttons = document.getElementById('infoPopUp');
 		$scope.Bar.footerOpen = false;
		console.log("ININ")
        move(buttons)
        .ease('in-out')
        .y(0)
        .duration('0.5s')
        .end();
		
		 $timeout(function() {
			 	
				//$scope.Bar.open = false;
			}, 500);
    };
	
	$scope.moveInfoPopUpUp = function() {
		$scope.Bar.open = true;
		var marginY=window.innerHeight/1.9494845360824741;
		marginY=marginY*(-1);
		$scope.Bar.footerOpen = true;
        var buttons = document.getElementById('infoPopUp');
        move(buttons)
        .ease('in-out')
        .y(marginY)
        .duration('0.5s')
        .end();
		
		
    };
	
	$scope.getReviewDate = function(item)
	{
		//console.log("DT " ,item.date )
		var DT = item.date.split(' ');
		var DT1 = DT[0].split('-');
		var DT2 = DT1[2]+"/"+DT1[1]+"/"+DT1[0];
		
		return DT2;
	}
	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		send_params = 
		{
			
		}
					

		//$http.post($rootScope.Host+'/GetSuppliers',send_params)
		$http.post($rootScope.Host+'/GetCategories',send_params)	
		.success(function(data, status, headers, config)
		{
			console.log("Cat : " , data)
			$scope.categories = data;
			
			$scope.categories = $scope.categories.reverse();
		setTimeout(function() {
			$ionicScrollDelegate.$getByHandle('daysScroll').scrollTo(-300,0,true);
			},200);
		});
		
		
	
	
	$scope.openPage = function(URL)
	{
		iabRef = window.open(URL, '_blank', 'location=yes');
	}
	
	// 0 = phone , 1 = message supplier
	$scope.countAction = function(type)
	{
		if (type == 0)
			$scope.action = "phone";
		else
			$scope.action = "message";
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.SupplierInfo.index,
			"type" : $scope.action
		}
					

		$http.post($rootScope.Host+'/CountSupplierStats',send_params)
		.success(function(data, status, headers, config)
		{
			//alert ("ok");

		})
		.error(function(data, status, headers, config)
		{

		});	


							
		
	}
	
	$scope.dialPhone = function(phone)
	{
		
		window.location ="tel://"+phone;
		$scope.countAction(0);
	}
	
	$scope.openReviews = function()
	{
		if ($scope.showReviews)
		{
			$scope.showInfo = true;
			$scope.showReviews = false;			
		}
		else
		{
			$scope.showInfo = false;
			$scope.showReviews = true;			
		}

	}


	$scope.setCatagory= function(id)
	{
		$scope.selectedCategory = id;
		$scope.showMyLocation();
		$rootScope.DefaultCatagory = id;
		$scope.showFindPro = true;
		$scope.fields.showdetails  = false;
		$scope.showProfession = false;
	}
	
	
	

	

	
	$scope.setGoogleMap = function(lat,lng)
	{
		var myLatlng = new google.maps.LatLng(lat,lng);
		var mapOptions = {
			center: myLatlng,
			zoom: 16,
			styles:[{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#C6E2FF"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#C5E3BF"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#D1D1B8"}]}],
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map"), mapOptions);
		map.setCenter(new google.maps.LatLng(lat,lng));
		var myLocation = new google.maps.Marker({
			position: new google.maps.LatLng(lat,lng),
			map: map,
			//http://maps.google.com/mapfiles/ms/icons/blue-dot.png
			icon : "img/icon-pin-location.png",
			animation: google.maps.Animation.DROP,
			title: "המיקום שלי",
			
		});	


		
	}	
	

	$scope.setCurrentLocation = function()
	{
		 $scope.showAddLocation = false;
		 $scope.fields.customsearch = 0;
		 $scope.fields.newlocation = "";
		 $scope.showMyLocation();
	}
	
	$scope.showMyLocation = function()
	{
		//regular search near by
		if ($scope.fields.customsearch == 0)
		{


		 
	
		  var posOptions = {timeout: 10000, enableHighAccuracy: false};
		  $cordovaGeolocation
			.getCurrentPosition(posOptions)
			.then(function (position) {
			  var lat  = position.coords.latitude
			  var long = position.coords.longitude
				  
				$scope.fields.location_lat = lat;
				$scope.fields.location_lng = long;
				
				$scope.getLocationName($scope.fields.location_lat,$scope.fields.location_lng);
				$scope.setGoogleMap($scope.fields.location_lat,$scope.fields.location_lng)

			  
			}, function(err) {
			  // error
			});
			
			
		}
		//custom search 
		else
		{
				$scope.getLocationName($scope.fields.location_lat,$scope.fields.location_lng);
				$scope.setGoogleMap($scope.fields.location_lat,$scope.fields.location_lng)			
		}

		

	}
  
	$scope.showMyLocation();
	$scope.setCatagory(1);

	
	$scope.getSuppliers = function(id)
	{
		//$rootScope.DefaultCatagory = $rootScope.DefaultCatagory;
		//$scope.moveInfoPopUp();

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		send_params = 
		{
			"user" : $localStorage.userid,//$localStorage.userid,
			"catagory" : $rootScope.DefaultCatagory,
			"lat" : $scope.fields.location_lat,
			"lng" : $scope.fields.location_lng
		}
		
		console.log("getSuppliers")
		//alert ($rootScope.DefaultCatagory);
		//return;

		//$http.post($rootScope.Host+'/GetSuppliers',send_params)
		$http.post($rootScope.PHPHost+'/getSuppliers.php',send_params)		
		.success(function(data, status, headers, config)
		{
			console.log("getSuppliers",data);
			//$scope.Catagories = data;
			$rootScope.SuppliersData = data;
			$scope.suppliersCount = data.length;
			$scope.showProfession = true;
			$scope.showFindPro = false;
			$ionicSlideBoxDelegate.update();
			$timeout(function(){
      				$ionicSlideBoxDelegate.update();
				},100);
			//console.log(data);	
			//$scope.showSuppliers(id);	
			
			//alert (index);
			$scope.showHidden = 1;
			//$scope.CatagoryIndex = $rootScope.DefaultCatagory;
			//console.log("ss1 " , $scope.Catagories)
			$scope.dataInfo = data;

 			
			

			// Map Settings //
			$scope.initialise = function() {
				var myLatlng = new google.maps.LatLng($scope.fields.location_lat,$scope.fields.location_lng);
				var mapOptions = {
					center: myLatlng,
					zoom: 13,
					styles:[{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#C6E2FF"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#C5E3BF"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#D1D1B8"}]}],
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById("map"), mapOptions);
			  // Geo Location /
			  

				var circle = new google.maps.Circle({
					center: myLatlng,
					map: map,
					radius: $rootScope.DefaultRadius*800,          // IN METERS.
					fillColor: '#61B5CF',
					fillOpacity: 0.3,
					strokeColor: "#3b7791",
					strokeWeight: 2

				});

				 var direction = 1;
				var rMin = $rootScope.DefaultRadius*800, rMax = $rootScope.DefaultRadius*1000;
				setInterval(function() {
					var radius = circle.getRadius();
					if ((radius > rMax) || (radius < rMin)) {
						radius=rMin;
						//direction *= -1;
					}
					circle.setRadius(radius + direction * 10);
				}, 50);
		  
				infowindow = new google.maps.InfoWindow({
					content: ''
				})

				

				  //alert(lat)
				  //alert (long)

					map.setCenter(new google.maps.LatLng($scope.fields.location_lat,$scope.fields.location_lng));
					var myLocation = new google.maps.Marker({
						position: new google.maps.LatLng($scope.fields.location_lat,$scope.fields.location_lng),
						map: map,
						//http://maps.google.com/mapfiles/ms/icons/blue-dot.png
						icon : "img/icon-pin-location.png",
						animation: google.maps.Animation.DROP,
						title: "My Location"
					});


		
			  /*
				navigator.geolocation.getCurrentPosition(function(pos) {

				});
				*/
				var infowindows = [];
				$scope.map = map;
				// Additional Markers //
				$scope.markers = [];
				/*
				var infoWindow = new google.maps.InfoWindow({
					content: 'Content goes here..'
				});
				*/

				var createMarker = function (index,info){
					var prev_infowindow =false; 
					//alert (info);
					//alert (index);
					//var marker = new google.maps.Marker({
					var marker = new MarkerWithLabel({
					position: new google.maps.LatLng(info.business_lat, info.business_lng),
				
				//   labelContent: info.name,
				   labelAnchor: new google.maps.Point(40, 90),
				   labelClass: "labels", // the CSS class for the label
				   labelStyle: {opacity: 1},
					//infoWindowIndex: index,
					//visible:true,
					map: $scope.map,
					//icon : $rootScope.PHPHost+info.image,
					//tooltip: '<B>test</B>',
					icon: new google.maps.MarkerImage(
					$rootScope.PHPHost+info.image, // my 16x48 sprite with 3 circular icons
					null, // desired size
					null, // offset within the scaled sprite
					null, // anchor point is half of the desired size
					new google.maps.Size(60, 60) // scaled size of the entire sprite
				   ),
					optimized:false,
				
					//shape:{coords:[17,17,18],type:'circle'},
					animation: google.maps.Animation.DROP,
					//title: info.name
					});
					
		
					/*
					// Add circle overlay and bind to marker
					var circle = new google.maps.Circle({
					  map: map,
					  radius: 16093,    // 10 miles in metres
					  fillColor: '#AA0000'
					});
					circle.bindTo('center', marker, 'position');
					*/
					
		
					//infoWindow.open(map, marker);

					
					 // I create an OverlayView, and set it to add the "markerLayer" class to the markerLayer DIV
					 var myoverlay = new google.maps.OverlayView();
					 myoverlay.draw = function () {
						 this.getPanes().markerLayer.id='markerLayer';
					 };
					 myoverlay.setMap(map);

		 
					//var htmlElement = '<div class="infowindow">sadad</div>'
					//var compiled = $compile(htmlElement)($scope);


					//marker.content = '<div class="infoWindowContent" >' + info.address + '</div>';
					

					var content = '<div ng-click="navigateDetails('+index+')">'+
					'<h1>'+info.name+'</h1>'+
					'<div>'+info.catagoryname+'</div>'+
					'<div ><img src="'+$rootScope.PHPHost+info.image+'" style="width:60px; height:60px;"></div>'+
					'</div>';
					
					var compiledContent = $compile(content)($scope)
					
						
						
					google.maps.event.addListener(marker, 'click', function() 
					{
						$scope.fields.showdetails = true;	
						$scope.fields.supplierindex = index;	
						$scope.showSupplierPopup(index,info);
						$scope.$apply();					
					});

		
					/*
					google.maps.event.addListener(marker, 'click', (function(marker, content, scope) {
						return function() {
							
							$scope.showSupplierPopup(index,info);
							$scope.showPopUp = true;
							//infowindow.setContent(content);
						   //infowindow.open(map, marker);
						};
					})(marker, compiledContent[0], $scope));
					*/
				
					
					//$scope.markers.push(marker);
				}  
				
				$scope.isFirstMarker = 0;
				for(var i=0;i< $scope.dataInfo.length;i++)
				{
					if ($scope.dataInfo[i].business_lat && $scope.dataInfo[i].business_lng)
					{
						createMarker(i,$scope.dataInfo[i]);
						
						if($scope.isFirstMarker == 0)
						{
							$scope.fields.showdetails = true;	
							$scope.fields.supplierindex = i;	
							$scope.showSupplierPopup(i,$scope.dataInfo[i]);
							//	$scope.$apply();
							$scope.isFirstMarker = 1;
						}
					}
				}	
				


					
				//for (i = 0; i < cities.length; i++){
					//createMarker(cities[i]);
				//}

			};
			
			
			google.maps.event.addDomListener(document.getElementById("map"), 'load', $scope.initialise());

			
			//for(var i=0;i< $scope.dataInfo.length;i++)
			//{
				//alert ($scope.dataInfo[i].name)
			//}

			$scope.navigateDetails(0,0)
			//console.log("suppliers: " , $scope.dataInfo )

		})
		.error(function(data, status, headers, config)
		{

		});	


							
		/*
		$http.get($rootScope.Host+'GetSuppliers').success(function(data)
		{
		});		
		*/
		
	}

	
	

	$scope.showSupplierPopup = function(index,item)
	{
		//alert ($rootScope.SuppliersData)
		//alert (index);
		//$rootScope.selectedIndex = index;
		console.log(" showSupplierPopup " , index , item)
		$scope.SupplierInfo = item;
		
		
		//$scope.SupplierInfo = $rootScope.SuppliersData[index];
		//console.log("SELECTED " ,  $scope.SupplierInfo)
		
		
		if ($scope.SupplierInfo.comments)
			$scope.Comments = $scope.SupplierInfo.comments;
		else
			$scope.Comments = [];

		
		
		/*$scope.fields.showdetails = true;
		//$scope.$apply();
		$scope.popupIndex = index;
		$scope.popupImage = $rootScope.PHPHost+item.image;
		$scope.popupSupplierName = item.name;
		$scope.popupSupplierDetails = item.desc;
		$scope.popupSupplierShortDetails = "";
		$scope.popupDistance = String(item.distance);
		$scope.popupSupplierCatagory = item.catagoryname;*/
	}
	

	$scope.openSearchModal = function()
	{		
		console.log("openSearchModal")
	   $ionicModal.fromTemplateUrl('templates/search_modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(searchModal) {
		  $scope.searchModal = searchModal;
		  $scope.searchModal.show();
		});
		
	}
	$scope.closeSearchModal = function()
	{
		$scope.searchModal.hide();
	}
	
	$scope.navigateDetails = function(index,type)
	{
		//alert (index);
		$rootScope.selectedIndex = index;
		
		$scope.SupplierInfo = $rootScope.SuppliersData[index];
		console.log("SELECTED " ,  $scope.SupplierInfo)
		
		
		if ($scope.SupplierInfo.comments)
			$scope.Comments = $scope.SupplierInfo.comments;
		else
			$scope.Comments = [];
		
		
		
		//alert ($scope.SupplierInfo)
		if(type == 1)
		{
			$scope.moveInfoPopUpUp();
			$scope.Bar.open = true;
			$scope.Bar.footerOpen = true;
		}
		
	 /*  $ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		});*/

	
	}
		
		
   /* $scope.closeModal = function() {
      $scope.modal.hide();
    };*/
	
	
	$scope.ChangeSupplier = function(type)
	{
		//alert ($rootScope.selectedIndex)
		$scope.nextSupplier =  $rootScope.selectedIndex;
		$scope.totalSuppliers = $scope.suppliersCount-1;
		
		console.log("suppliersCount",$scope.totalSuppliers)
		console.log("nextSupplier",$scope.nextSupplier);
		$scope.ToggleNext = false;
		console.log("TP : " + type + " : " + $scope.totalSuppliers + " : " + $scope.nextSupplier)
		if (type == 0)
		{
			if ($scope.totalSuppliers > $scope.nextSupplier)
			{
				$scope.ToggleNext = true;
				$scope.nextSupplier++;
			}
			else
			{
				//alert ("bad");
				$scope.ToggleNext = false;
			}
		}
		else if ($scope.totalSuppliers > 0 &&  $scope.nextSupplier > 0)
		{
			$scope.ToggleNext = true;
			$scope.nextSupplier--;
		}
		else
		{
			//alert ("bad");
			$scope.ToggleNext = false;
		}

		
		
		
		//if (type == 0)
			//$scope.nextSupplier = $rootScope.selectedIndex++;
		//else
			//$scope.nextSupplier = $rootScope.selectedIndex--;
		

		
		//if ($scope.ToggleNext)
		//$scope.closeModal();		
		
		console.log("ToggleNext",$scope.ToggleNext);
		
		if ($scope.ToggleNext)
		$scope.navigateDetails($scope.nextSupplier,0);
	
	}
	
	
	$scope.addLocationBtn = function()
	{
		$scope.fields.newlocation = "";
		$scope.showAddLocation = true;
	}
	
	$scope.getLocationName = function(lat,lng)
	{
		$http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&sensor=true').then(function(data)
		{   
		console.log("location: ", data);
		//$scope.fields.city = data.data.results[0].address_components[2].long_name;	
		$scope.fields.locationname = data.data.results[0].formatted_address;
		//alert ($scope.fields.locationname)
		//alert ($scope.fields)
		}, function(err) {
		  // error
		});		
	}
	
	$scope.saveSearchLocation = function()
	{
		if ($scope.fields.newlocation)
		{
			$scope.formattedaddress = $scope.fields.newlocation.formatted_address;
			$scope.newaddress = String($scope.fields.newlocation.geometry.location);
			$scope.split = $scope.newaddress.split(",");
			$scope.locationx = $scope.split[0].replace("(", "");
			$scope.locationy = $scope.split[1].replace(")", "");
			
			if ($scope.locationx && $scope.locationy)
			{
				$scope.fields.locationname = $scope.formattedaddress;
				$scope.fields.location_lat = $scope.locationx;
				$scope.fields.location_lng = $scope.locationy;
				$scope.fields.customsearch = 1;
				$scope.setGoogleMap($scope.fields.location_lat,$scope.fields.location_lng);				
			}
			
			
			//alert ($scope.formattedaddress)
			//alert ($scope.locationx)
			//alert ($scope.locationy)			
		}
		$rootScope.DefaultRadius = $scope.fields.searchradious;
		$scope.showAddLocation = false;
		$scope.showFindPro = true;
		$scope.fields.showdetails = false;
		$scope.getSuppliers();
		$scope.closeSearchModal();
	
	}

					

	
	$scope.startChat = function()
	{
			   //alert ($scope.SupplierInfo.phone)

			   $scope.updatedInfoPopup = $ionicPopup.show({
				   templateUrl: 'templates/contact_supplier.html',
				   scope: $scope,
				   cssClass: 'SupplierContact'
			   });
			   
			   ClosePopupService.register($scope.updatedInfoPopup);
			   
			   
							   
				/*			   
			   $ionicPopup.show({
				   //templateUrl: 'templates/popup_comment.html',
				//template: '<textarea rows="4" cols="50" ng-model="fields.textmsg" ></textarea>',
				//title: 'Message '+$scope.SupplierInfo.name,
				//scope: $scope,

			  });


			  
			  
			   $ionicPopup.show({
				template: '<textarea rows="4" cols="50" ng-model="fields.textmsg" ></textarea>',
				title: 'Message '+$scope.SupplierInfo.name,
				scope: $scope,
				buttons: [
				  { text: 'Cancel' },
				  {
					text: '<b>Send Message</b>',
					type: 'button-positive',
					onTap: function(e) {
						if ($scope.fields.textmsg)
						{
							
							
							$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
							
							send_params = 
							{
								"user" : $localStorage.userid,
								"name" : $localStorage.name,
								"id" : $scope.SupplierInfo.index,
								"reciver" : $scope.SupplierInfo.user_id,
								"msg" : $scope.fields.textmsg,
								"send" : 1
							}
										

							$http.post($rootScope.Host+'/SendSupplierMsg',send_params)
							.success(function(data, status, headers, config)
							{
								$ionicPopup.alert({
									 title: 'Your message was successfully sent to '+$scope.SupplierInfo.name,
									 template: ''
								   });	

							})
							.error(function(data, status, headers, config)
							{

							});	

							$scope.fields.textmsg = '';
						}
					}
				  }
				]
			  });
			*/
	}

	$scope.closeContact = function()
	{
		$scope.updatedInfoPopup.close();
	}
	
	
  $scope.onSuccess = function()
  {
		$ionicPopup.alert({
		title: 'הודעה נשלחה בהצלחה',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });
  }  

  $scope.onError = function()
  {
		$ionicPopup.alert({
		title: 'שגיאה בשליחת הודעה יש לנסות שוב',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });
  }  
  
	$scope.msgWhatsapp = function()
	{

	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לשלוח הודעה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			$scope.countAction(1);	
			
			window.plugins.socialsharing.available(function(isAvailable) {
			  // the boolean is only false on iOS < 6
			  if (isAvailable) {
					window.plugins.socialsharing.shareViaWhatsAppToReceiver($scope.SupplierInfo.phone, 'שלום, הופניתי אלייך דרך אפליקציית WhoCan. אשמח ליצירת קשר חזרה', null /* img */, null /* url */),
				  $scope.onSuccess(), // optional success function
				  $scope.onError()   // optional error function
			  }
			});

					
		} 
	   });

	   
		

	}
	
	$scope.msgSms = function()
	{
		if ($scope.SupplierInfo.phone)
		{


	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לשלוח הודעה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			$scope.countAction(1);	
			
			var options = {};

		
			$cordovaSms
			  .send($scope.SupplierInfo.phone, 'שלום, הופניתי אלייך דרך אפליקציית WhoCan. אשמח ליצירת קשר חזרה', options)
			  .then(function() {
				$scope.onSuccess();
			  }, function(error) {
				$scope.onError();
			  });

					
		} 
	   });
	   
		}


	}

	
	var comentPopup;
	
	$scope.commentModal = function()
	{
		 // $scope.rate = {};
			comentPopup = $ionicPopup.show({
                        templateUrl: 'templates/popup_comment.html',
                        scope: $scope,
                        cssClass: 'pointsPopup'
            });
					
				 
		  // An elaborate, custom popup
		 /*  $ionicPopup.show({
			template: '<textarea rows="4" cols="50" ng-model="fields.comment" ></textarea>',
			title: 'Comment on  '+$scope.SupplierInfo.name,
			scope: $scope,
			buttons: [
			  { text: 'Cancel' },
			  {
				text: '<b>Comment</b>',
				type: 'button-positive',
				onTap: function(e) {
					if ($scope.fields.comment)
					{
						
						$scope.date = new Date()
						$scope.hours = $scope.date.getHours()
						$scope.minutes = $scope.date.getMinutes()
					
						if ($scope.hours < 10)
						$scope.hours = " " + $scope.hours
						
						if ($scope.minutes < 10)
						$scope.minutes = "0" + $scope.minutes
					
						$scope.time = $scope.hours+':'+$scope.minutes;

					
						$scope.newmessage  = {
							"username" : $localStorage.name,
							"text" : $scope.fields.comment,
							"facebookid" : $localStorage.facebookid,
							"time" : $scope.time

						}
						$scope.Comments.unshift($scope.newmessage);

						
						
						
						
							$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
							
							send_params = 
							{
								"user" : $localStorage.userid,
								"provider" : $scope.SupplierInfo.index,
								"text" : $scope.fields.comment,
								"time" : $scope.time
							}
										

							$http.post($rootScope.Host+'/NewCommment',send_params)
							.success(function(data, status, headers, config)
							{


							})
							.error(function(data, status, headers, config)
							{

							});							
						
						
							$scope.fields.comment = '';

					}
				}
			  }
			]
		  });		*/
	}
	
	$scope.closeCommentPopup = function()
	{
		console.log("Comment " ,$scope.fields.comment )
		if ($scope.fields.comment)
		{
			//alert ($scope.commentRating);
			//return;
			
			
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
		
			if ($scope.hours < 10)
			$scope.hours = " " + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
		
			//$scope.time = new Date().format("%Y-%m-%d").toString(); ; //$scope.hours+':'+$scope.minutes;
			var today = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss Z');
		
			$scope.newmessage  = {
				"username" : $localStorage.name,
				"text" : $scope.fields.comment,
				"rating" : $scope.commentRating,
				"facebookid" : $localStorage.facebookid,
				"date" : today

			}
			$scope.Comments.unshift($scope.newmessage);

			console.log("Comment1 " ,$scope.newmessage )
			
			
			
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
				
				
				//alert ($scope.SupplierInfo.index);
				//alert ($localStorage.userid);
				
				
				send_params = 
				{
					"user" : $localStorage.userid,
					"provider" : $scope.SupplierInfo.index,
					"text" : $scope.fields.comment,
					"rating" : $scope.commentRating,
					"time" : today //$scope.time
				}
							

				$http.post($rootScope.Host+'/NewCommment',send_params)
				.success(function(data, status, headers, config)
				{
					
				})
				.error(function(data, status, headers, config)
				{

				});							
			
			
				$scope.fields.comment = '';

		}
		
		$scope.commentRating = 0;
		comentPopup.close();	
	}

	$scope.setCommentRating = function(rate)
	{
		$scope.commentRating = rate;
	}
	
	$scope.reportComment = function(id)
	{
		
		
		$scope.reportPopup = $ionicPopup.show({
					templateUrl: 'templates/report_comment.html',
					scope: $scope,
					cssClass: 'pointsPopup'
		});
		

		$scope.reportId = id;
		

			
		/*
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לדווח תגובה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

		} 
	   });
	   
	   */
	}


	$scope.sendReport = function()
	{
		
		
		if ($scope.fields.reporttext)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			

			
			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.reportId,
				"text" :  $scope.fields.reporttext
			}
						

			$http.post($rootScope.Host+'/ReportComment',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
				 title: 'תגובה דווחה בהצלחה',
				 template: ''
				});					
			})
			.error(function(data, status, headers, config)
			{

			});	
		}
		
		
		
		$scope.reportPopup.close();	
	}

		
})

.controller('InfoCtrl', function($scope,$rootScope,$http,$stateParams) {

  $scope.ItemId = $stateParams.ItemId;
  $scope.SupplierData = $rootScope.SupplierData[$scope.ItemId];
  //alert ($scope.SupplierData)
		
		
})


.controller('ProviderCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage,$ionicLoading) {
	
	$scope.navTitle= '<p class="Headtitle">הרשמת בית עסק<p>'
	
	/*
	$scope.Catagories = new Array();
	$scope.Catagories[1] = "Electrician";
	$scope.Catagories[2] = "HandyMan";
	$scope.Catagories[3] = "Movers";
	$scope.Catagories[4] = "Plumber";
	$scope.Catagories[5] = "PC Tech";
	*/

	$scope.getCatagories = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
		}
		//console.log(login_params)
		$http.post($rootScope.Host+'/GetCategories',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.Catagories = data;
			console.log("catagories: " , data);

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	$scope.getCatagories();



			

	//$scope.CatagoriesJson = '[{"id":"1","name":"חשמלאים"},{"id":"3","name":"שיפוצים"},{"id":"4","name":"מובילים"},{"id":"5","name":"אינסטלטור"},{"id":"6","name":"הייטק"}]';
	//$scope.Catagories = JSON.parse($scope.CatagoriesJson);
	//alert ($scope.Catagories[0].name)
					   
	//alert ($scope.phpHost)
	//$scope.Catagories = $rootScope.CatagoriesData;
	$scope.defaultPicture = "img/avatar.png";
	//$scope.image = "";
	$scope.phpHost = $rootScope.PHPHost;
	console.log("Proveider : " , $scope.Catagories);
	$scope.formStep = 1;
	$scope.codeSent = 0;


	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"address" : "",
		"location_lat" : "",
		"location_lng" : "",
		"website" : "",
		"facebook" : "",
		"desc" : "",
		"category" : "1",
		"verifycode" : ""
		//"city" : "1"
	}
	
	
	
	
	
	$scope.changeStep = function(step)
	{
		
		if ($scope.validateFields())
		{
			if ($scope.formStep  < 3)
			{
				if ($scope.formStep == 1)
				{
					
					$ionicLoading.show({
					  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
					});
				
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


					send_params = 
					{
						"user" : $localStorage.userid,
						"phone" : $scope.fields.phone
					}
					
					$http.post($rootScope.Host+'/CheckUserPhone',send_params)
					.success(function(data, status, headers, config)
					{
						$ionicLoading.hide();
						
						if (data[0].status == 0)
						{
							$scope.formStep++;
						}
						else
						{
							$ionicPopup.alert({
								 title: 'מצטערים הטלפון שהזנת כבר מוגדר כנותן שירות , יש לנסות שוב',
								 //template: ''
							 });	
							 $scope.fields.phone = '';
						}
					})
					.error(function(data, status, headers, config)
					{
						$ionicLoading.hide();
					});					
					
				}
				
				if ($scope.formStep == 2)
				{
					$scope.formStep++;
				}
				

		
			}
			
				if ($scope.formStep == 3)
				{
					console.log("address: ", $scope.fields.address);
					
					if ($scope.codeSent == 0)
					{
						$scope.sendCodeRegisterProvider();
					}
					
					
					if ($scope.fields.verifycode)
					{
						$ionicLoading.show({
						  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
						});
					
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


						send_params = 
						{
							"user" : $localStorage.userid,
							"phone" : $scope.fields.phone,
							"verifycode" : $scope.fields.verifycode,
						}
						
						$http.post($rootScope.Host+'/VerifyProviderCode',send_params)
						.success(function(data, status, headers, config)
						{
							$ionicLoading.hide();
							
							if (data[0].status == 0)
							{
								$ionicPopup.alert({
								 title: 'קוד שגוי יש לנסות שוב',
								 template: ''
								});									
							}
							else
							{


								$scope.formStep++;		
							}
						})
						.error(function(data, status, headers, config)
						{
							$ionicLoading.hide();
						});	
					}
				}
				
				
				if ($scope.formStep == 4)
				{
					$scope.formStep++;		
				}

				if ($scope.formStep == 5)
				{
					
					$scope.fields.name = '';
					$scope.fields.phone = '';
					$scope.fields.address = '';
					$scope.fields.location_lat = '';
					$scope.fields.location_lng = '';
					$scope.fields.website = '';
					$scope.fields.facebook = '';
					$scope.fields.desc = '';
					$scope.fields.category = "1";
					$scope.fields.verifycode = '';	

					$localStorage.provider = 1;
					$scope.showProviderFun();					
					window.location = "#/app/main";
					$scope.image = "";
					$scope.formStep = 1;
				}
				
				
		}
			
	}
	

	
	
	
	$scope.validateFields = function()
	{
		if ($scope.fields.name == "")
		{
				$ionicPopup.alert({
				 title: 'יש להזין שם העסק',
				 template: ''
			   });	
			   
			//return false;
		}
		else if ($scope.fields.phone == "")
		{
				$ionicPopup.alert({
				 title: 'יש להזין מספר טלפון העסק',
				 template: ''
			   });	
			   
			//return false;
		}	

		else if (!$scope.fields.address)
		{
			$ionicPopup.alert({
				 title: 'יש להזין כתובת העסק',
				 template: ''
			   });	
			//return false;	
		}		
		
		else if (!$scope.fields.address.geometry.location)
		{
			$ionicPopup.alert({
				 title: 'יש להזין כתובת העסק',
				 template: ''
			   });	
			   
			//return false;	
		}
		
		else if (!$scope.image)
		{
			  $ionicPopup.alert({
				 title: 'יש להעלות תמונה',
				 template: ''
			   });	
		}
		
		else if (!$scope.fields.verifycode && $scope.formStep == 3)
		{
			$ionicPopup.alert({
				 title: 'יש להזין קוד ווידוי',
				 template: ''
			   });	

			 //return false;  
		}
		
		else
		{
			return true;
		}
	}
	
	
	$scope.sendCodeRegisterProvider = function()
	{
		$scope.codeSent = 1;
		
		if ($scope.fields.phone)
		{
		
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});

				
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			$scope.formattedaddress = $scope.fields.address.formatted_address;
			$scope.newaddress = String($scope.fields.address.geometry.location);
			$scope.split = $scope.newaddress.split(",");
			$scope.locationx = $scope.split[0].replace("(", "");
			$scope.locationy = $scope.split[1].replace(")", "");
			//alert ($scope.locationx)
			//alert ($scope.locationy)
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"name" : $scope.fields.name,
				"phone" : $scope.fields.phone,
				"address" : $scope.formattedaddress,
				"location_lat" : $scope.locationx,
				"location_lng" :$scope.locationy,				
				"website" : $scope.fields.website,
				"facebook" : $scope.fields.facebook,
				"desc" : $scope.fields.desc,
				"category" : $scope.fields.category,
				"image" : $scope.image,
			}
			
			$http.post($rootScope.Host+'/sendVerificationCode',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				console.log("saved new supplier:" , data);
				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});					
		}
		//$scope.codeSent = 1;
	}
	
	
	$scope.sendSMSCode = function()
	{
		$scope.codeSent = 0;
		$scope.sendCodeRegisterProvider();
		
	}


	$scope.addressData22 = function()
	{
		console.log("address: ",$scope.fields.address);
		
		
		if ($scope.fields.address.id)
		{
			//alert (99);
			if ($scope.fields.address.geometry)
			{
				//alert ($scope.fields.address.geometry.location);
				console.log("geo: ", $scope.fields.address.geometry.location)
				if ($scope.fields.address.geometry.access_points[0].location.lat)
				{
					
					//alert (444);
					//alert ("address lat : "+$scope.fields.address.geometry.access_points[0].location.lat+ " address lng: "+ $scope.fields.address.geometry.access_points[0].location.lng)
				}	
			}	
		}
	}

	
	$scope.imageOptions = function()
	{

			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחירת מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	
	}
	
	
	$scope.addPictures = function()
	{
		
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'Select Image Source:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'CAMERA',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'PHOTO GALLERY',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	}

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			//UploadImg1.php
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/NewImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//console.log(data)
			 $timeout(function() {
				if (data.response)
				{
					$scope.image = data.response;
					//alert ($scope.image)
					$scope.defaultPicture = $rootScope.PHPHost+data.response;
					//alert ($scope.defaultPicture)					
				}

				
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	





})

.controller('TermsCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) {

	$scope.NavTitle= '<p class="Headtitle">תנאי שימוש<p>';
})

.controller('AboutCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) {

	$scope.NavTitle= '<p class="Headtitle">אודות<p>';

})


.controller('HowCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) {

	$scope.NavTitle= '<p class="Headtitle">איך זה עובד?<p>';

})

.controller('ContactCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) {

	$scope.NavTitle= '<p class="Headtitle">צור קשר<p>';
	
	$scope.contact = 
	{
		"email" : "",
		"name" : "",
		"subject" : "",
		"desc" : ""
	}

	
	$scope.sendContact = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.contact.email =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין כתובת מייל',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if (emailRegex.test($scope.contact.email) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת מייל לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.contact.email =  '';
		}
		else if ($scope.contact.subject =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין נושא הפניה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}

		else if ($scope.contact.desc =="")
		{
			$ionicPopup.alert({
			title: 'יש להזין תוכן ההודעה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}		
		else
		{
			
			send_params = 
			{
				"email" : $scope.contact.email,
				"name" : $scope.contact.name,
				"subject" : $scope.contact.subject,
				"desc" : $scope.contact.desc
			}
			
			$http.post($rootScope.Host+'/SendContact',send_params)
			.success(function(data, status, headers, config)
			{

					$ionicPopup.alert({
					title: 'תודה על פניתך לאפליקציית WhoCan , נחזור אליך בהקדם.',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });					
				

			})
			.error(function(data, status, headers, config)
			{

			});	
			
			$scope.contact.email =  '';
			$scope.contact.name  = '';
			$scope.contact.subject =  '';
			$scope.contact.desc =  '';
		}
		
	}
})


.controller('WorkingDaysCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) {
	
	$scope.navTitle= '<p class="Headtitle">הגדרות<p>';
	
	$scope.defaultDay = 0;

	$scope.days = new Array();
	$scope.days[0] = "Sun";
	$scope.days[1] = "Mon";
	$scope.days[2] = "Tue";
	$scope.days[3] = "Wed";
	$scope.days[4] = "Thu";
	$scope.days[5] = "Fri";
	$scope.days[6] = "Sat";
	
	$scope.fields = 
	{
		"day" : $scope.defaultDay,
		"start" :"9:00",
		"end" : "17:00",
		"active" : false
	}
		
		
	$scope.getSupplierWorkingDays = function(day)
	{
		console.log("sss1");
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		$scope.Days = [];
		
		send_params = 
		{
			"user" : $localStorage.userid,
			"day" : day
		}
		//console.log(login_params)
		$http.post($rootScope.Host+'/GetSupplierWorkingDays',send_params)
		.success(function(data, status, headers, config)
		{
			console.log("sss2");
			
			
			for(var i=0;i<data.length; i++)
			{
				if(data[i].day == 0)
					dayName = "ראשון";
				if(data[i].day == 1)
					dayName = "שני";
				if(data[i].day == 2)
					dayName = "שלישי";
				if(data[i].day == 3)
					dayName = "רביעי";
				if(data[i].day == 4)
					dayName = "חמישי";
				if(data[i].day == 5)
					dayName = "שישי";
				if(data[i].day == 6)
					dayName = "שבת";
					
				data[i].dayName = dayName;
			}
			
			$scope.Days = data;
			
			if  (data[0].status == 1)
			{
				$scope.fields.start = data[0].start;
				$scope.fields.end = data[0].end;
				
				if (data[0].active == 1)
					$scope.fields.active = true;
				else
					$scope.fields.active = false;
			}

		})
		.error(function(data, status, headers, config)
		{

		});	
	}		
	//console.log("sss");
	
	//$scope.getSupplierWorkingDays(2)
	
	$scope.changeDay = function(day)
	{
		$scope.defaultDay = day;
		$scope.SelectedDay = $scope.days[day];
		$scope.getSupplierWorkingDays(day);
	}
	
	$scope.changeDay(0);



	
	
	$scope.saveDays = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		
		send_params = 
		{
			"user" : $localStorage.userid,
			"day" : $scope.defaultDay,
			"start" : $scope.fields.start,
			"end" : $scope.fields.end,
			"active" : $scope.fields.active
		}
		
		//console.log(login_params)
		$http.post($rootScope.Host+'/SaveWorkingDays',send_params)
		.success(function(data, status, headers, config)
		{

			$ionicPopup.alert({
			title: 'נשמר בהצלחה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
			
		})
		.error(function(data, status, headers, config)
		{

		});

		

	}
	
	$scope.setActiveDay = function(day)
	{	
		if($scope.Days[day].active == 0)
			$scope.Days[day].active  = 1;
		else
			$scope.Days[day].active  = 0;
		
		send_params = 
		{
			"user" : 12,//$localStorage.userid,
			"day" : $scope.Days[day].day,
			"active" : $scope.Days[day].active
		}
		
		console.log("Params : " , send_params)
		$http.post($rootScope.Host+'/UpdateActiveDay',send_params)
		.success(function(data, status, headers, config)
		{
		   console.log("saveDays", data)		

		})
		.error(function(data, status, headers, config)
		{

		});
		
		console.log("Day : " , $scope.Days[day].active)
	}

})


.controller('AvailableCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage,$ionicLoading) 
{
	//$scope.navTitle= '<img class="title-image" src="img/headerlogo.png" style="width:140px; margin-left:-15px; margin-top:2px;"/>';
	$scope.navTitle= '<p class="Headtitle">פרופיל עסק <p>';
	$scope.isAvaileble = $localStorage.available;
	
	//alert ($scope.isAvaileble);
	
	$scope.availebleClicked = function(type)
	{
		

		
		
		
		//console.log ($scope.isAvaileble);
		//return;

		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"available" : !type
		}
		
		$http.post($rootScope.Host+'/UpdateAvailable',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			

			if($scope.isAvaileble == 1)
				$scope.isAvaileble = 0; 
			else
				$scope.isAvaileble = 1;

			$localStorage.available = $scope.isAvaileble;
		
		

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
		

	}
})


.controller('MyProfileCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) 
{
	$scope.NavTitle= '<p class="Headtitle">פרופיל<p>';
	$scope.HttpHost = $rootScope.PHPHost;
})	

.controller('EditProfileCtrl', function($scope,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage) 
{
	$scope.NavTitle= '<p class="Headtitle">עריכת עסק<p>';
	
	
	$scope.fields = 
	{
		"id" : "",
		"name" : "",
		"phone" : "",
		"address" : "",
		"website" : "",
		"facebook" : "",
		"category" : "",
		"desc" : "",
		"image" : "",
		"location_lat" : "",
		"location_lng" : "",		
	}
	
	
	$scope.getCatagories = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
		}
		//console.log(login_params)
		$http.post($rootScope.Host+'/GetCategories',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.Catagories = data;
			console.log("catagories: " , data);

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	$scope.getCatagories();
	
	
	//alert($localStorage.userid);

	$scope.getSupplier = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/GetSupplier',send_params)
			.success(function(data, status, headers, config)
			{
				 $timeout(function() 
				 {

					$scope.fields.id = data[0].index;
					$scope.fields.name = data[0].name;
					$scope.fields.phone = data[0].phone;
					$scope.fields.address = data[0].address;
					$scope.fields.website = data[0].website;
					$scope.fields.facebook = data[0].facebook;
					$scope.fields.category = data[0].category;
					$scope.fields.desc = data[0].desc;

					$scope.fields.location_lat = data[0].business_lat;
					$scope.fields.location_lng = data[0].business_lng;

					
					$scope.fields.image = data[0].image;
					
					$scope.defaultPicture = $rootScope.PHPHost+'/'+data[0].image;
					
				}, 300);
			
			})
			.error(function(data, status, headers, config)
			{

			});	
	}
	
	$scope.getSupplier();
	
	$scope.imageOptions = function()
	{

			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחירת מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	
	}



	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			//UploadImg1.php
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/NewImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//console.log(data)
			 $timeout(function() {
				if (data.response)
				{
					$scope.newimage = data.response;
					//alert ($scope.image)
					$scope.defaultPicture = $rootScope.PHPHost+data.response;
					//alert ($scope.defaultPicture)					
				}

				
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }

	
	
	$scope.saveProfile = function()
	{
		if ($scope.newimage)
			$scope.saveimage = $scope.newimage;
		else
			$scope.saveimage = $scope.fields.image;
		
		//alert ($scope.fields.id);
		//return;
		
		if ($scope.fields.address.formatted_address)
			$scope.address = $scope.fields.address.formatted_address;
		else
			$scope.address = $scope.fields.address;
		
		
		if ($scope.fields.address.formatted_address)
		{
			$scope.newaddress = String($scope.fields.address.geometry.location);
			$scope.split = $scope.newaddress.split(",");
			$scope.locationx = $scope.split[0].replace("(", "");
			$scope.locationy = $scope.split[1].replace(")", "");
			
			$scope.fields.location_lat = $scope.locationx;
			$scope.fields.location_lng = $scope.locationy;	
		}

			
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.fields.id,
				"name" : $scope.fields.name,
				"phone" : $scope.fields.phone,
				"address" : $scope.address,
				"website" : $scope.fields.website,
				"facebook" : $scope.fields.facebook,
				"category" : $scope.fields.category,
				"desc" : $scope.fields.desc,
				"image" : $scope.saveimage,
				"location_lat" : $scope.fields.location_lat,	
				"location_lng" : $scope.fields.location_lng					
			}

			//console.log(login_params)
			$http.post($rootScope.Host+'/SaveSupplier',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
				title: 'נשמר בהצלחה',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });	
			
			})
			.error(function(data, status, headers, config)
			{

			});	


			
		
		
	}

})	

.controller('RateSupplierCtrl', function($scope,$stateParams,$http,$rootScope,$state,$ionicPopup,ClosePopupService,$cordovaCamera,$timeout,$localStorage,$filter) 
{
	$scope.NavTitle= '<p class="Headtitle">דירוג נותן שירות<p>';
	$scope.itemId = $stateParams.ItemId;
	//alert ($scope.itemId)
	$scope.commentRating = 0;
	$scope.PhpHost = $rootScope.PHPHost;
	
	$scope.setCommentRating = function(rate)
	{
		//alert (rate);
		$scope.commentRating = rate;
	}	
	
	$scope.fields = 
	{
		"comment" : ""
	}
	
	
	$scope.getSupplierInfo = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

						
						
		send_params = 
		{
			"provider" : $scope.itemId,
		}
					

		$http.post($rootScope.Host+'/GetSupplierData',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.SupplierData = data[0];
		})
		.error(function(data, status, headers, config)
		{

		});	
	}
	
	
	$scope.getSupplierInfo();
	
	
	
	
	
	$scope.saveRate = function()
	{
		if ($scope.commentRating == 0)
		{
			$ionicPopup.alert({
			title: 'יש לבחור דירוג',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	
		}

		else
		{
			/*
			if (!$scope.fields.comment)
			{
				$ionicPopup.alert({
				title: 'יש לתאר את הדירוג בכמה מילים',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });	
			}
			else
			{
				
			}
			*/
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
		
			if ($scope.hours < 10)
			$scope.hours = " " + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
		
			//$scope.time = new Date().format("%Y-%m-%d").toString(); ; //$scope.hours+':'+$scope.minutes;
			var today = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss Z');
		


			console.log("Comment1 " ,$scope.newmessage )
			
			
			
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
				
				
				//alert ($scope.SupplierInfo.index);
				//alert ($localStorage.userid);
				
				
				send_params = 
				{
					"user" : $localStorage.userid,
					"provider" : $scope.itemId,
					"text" : $scope.fields.comment,
					"rating" : $scope.commentRating,
					"time" : today //$scope.time
				}
							

				$http.post($rootScope.Host+'/NewCommment',send_params)
				.success(function(data, status, headers, config)
				{
					
					$ionicPopup.alert({
					title: 'נותן שירות דורג בהצלחה',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });						
					window.location = "#/app/main";
				})
				.error(function(data, status, headers, config)
				{

				});							
			
			
				$scope.fields.comment = '';
				
				
				
			
		}
		
		
		
	}
	

})	


.filter('range', function () {
    return function (input, total) 
	{
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }
	
	
        return input;
    };
})



.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
  var lastPopup;
  return {
    register: function(popup) {
      $timeout(function(){
        var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
        if(!element || !popup || !popup.close) return;
        element = element && element.children ? angular.element(element.children()[0]) : null;
        lastPopup  = popup;
        var insideClickHandler = function(event){
          event.stopPropagation();
        };
        var outsideHandler = function() {
          popup.close();
        };
        element.on('click', insideClickHandler);
        $document.on('click', outsideHandler);
        popup.then(function(){
          lastPopup = null;
          element.off('click', insideClickHandler);
          $document.off('click', outsideHandler);
        });
      });
    },
    closeActivePopup: function(){
      if(lastPopup) {
        $timeout(lastPopup.close);
        return lastPopup;
      }
    }
  };
})

